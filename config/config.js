const dotenv = require('dotenv');

dotenv.config();

module.exports={
    development: {
        username: 'root',
        password: process.env.DB_PASSWORD,
        database: 'freemed_test',
        host: process.env.MYSQL_HOST,
        dialect: 'mysql',
        port:3306
    },
    test: {
        username: 'root',
        password: process.env.DB_PASSWORD,
        database: 'freemed_test',
        host: process.env.MYSQL_HOST,
        dialect: 'mysql',
        port:3306
    },
    production: {
        username: 'root',
        password: process.env.DB_PASSWORD,
        database: 'freemed_test',
        host: process.env.MYSQL_HOST,
        dialect: 'mysql',
        port:3306
    }
};
